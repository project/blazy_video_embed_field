<?php

namespace Drupal\blazy_video_embed_field\Plugin\Field\FieldFormatter;

use Drupal\blazy\BlazyDefault;
use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyFormatterTrait;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Base class for blazy video embed field formatters.
 *
 * Ported from blazy 2.16+.
 */
abstract class BlazyVideoEmbedFieldFormatterBase extends FormatterBase implements ContainerFactoryPluginInterface {

  use BlazyFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'blazy';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'content';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'blazy';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'captions';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'use_oembed' => TRUE,
    ] + BlazyDefault::extendedSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $definition = $this->getScopedFormElements();
    $definition['_views'] = isset($form['field_api_classes']);
    $this->admin()->buildSettingsForm($element, $definition);
    $element['media_switch']['#options']['media'] = $this->t('Image to iFrame');

    $element['use_oembed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use oEmbed when applicable with a VEF fallback'),
      '#description' => $this->t('Enable to use oEmbed when there is a matching oEmbed provider. When there is not an oEmbed provider, then it uses the embed URL from the Video Embed Field provider. Example: JW Player does not have oEmbed support.'),
      '#default_value' => $this->getSetting('use_oembed'),
      '#weight' => -999,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('use_oembed')) {
      array_unshift($summary, $this->t('Embed Url: Use oEmbed with VEF fallback'));
    }
    else {
      array_unshift($summary, $this->t('Embed Url: Video Embed Field only'));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'background'        => TRUE,
      'image_style_form'  => TRUE,
      'media_switch_form' => TRUE,
      'multimedia'        => TRUE,
      'thumb_positions'   => TRUE,
      'nav'               => TRUE,
    ];
  }

  /**
   * Returns the optional VEF service to avoid dependency for optional plugins.
   *
   * @return \Drupal\video_embed_field\ProviderManagerInterface|null
   *   The VEF provider manager.
   */
  protected function vefProviderManager() {
    return \Drupal::hasService('video_embed_field.provider_manager') ? \Drupal::service('video_embed_field.provider_manager') : NULL;
  }

  /**
   * Returns the oEmbed url resolver.
   *
   * @return \Drupal\media\OEmbed\UrlResolverInterface|null
   *   The oEmbed url resolver.
   */
  protected function oEmbedUrlResolver() {
    return \Drupal::hasService('media.oembed.url_resolver') ? \Drupal::service('media.oembed.url_resolver') : NULL;
  }

}
