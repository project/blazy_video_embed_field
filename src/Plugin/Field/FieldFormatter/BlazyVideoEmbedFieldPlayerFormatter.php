<?php

namespace Drupal\blazy_video_embed_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formatter for 'Blazy Video' to get video_embed_field videos.
 *
 * Ported from blazy 2.16+ and then adapted for VEF with the following:
 * - Added a formatter setting to use oEmbed when the input matches an oEmbed
 *   provider.
 * - If the oEmbed setting is disabled OR there is no matching oEmbed provider,
 *   then the formatter falls back to use the VEF provider's embed URL.
 *   Example: JW Player does not have an oEmbed provider, however there is a
 *   VEF provider that supports JW Player with the video_embed_jwplayer module.
 *
 * For converting video_embed_field to core media oembed, the following patch
 * provides a drush command to convert:
 * - https://www.drupal.org/project/video_embed_field/issues/2997799
 *
 * @FieldFormatter(
 *   id = "blazy_vef_default",
 *   label = @Translation("Blazy Video Embed Field"),
 *   description = @Translation("Lazy load the video embed field."),
 *   field_types = {
 *     "video_embed_field"
 *   }
 * )
 */
class BlazyVideoEmbedFieldPlayerFormatter extends BlazyVideoEmbedFieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected static $fieldType = 'entity';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return static::injectServices($instance, $container, static::$fieldType);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if ($items->isEmpty()) {
      return [];
    }

    return $this->commonViewElements($items, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getType() === 'video_embed_field';
  }

  /**
   * Build the blazy elements.
   */
  protected function buildElements(array &$build, $items, $langcode) {
    $settings = $this->formatter->toHashtag($build);
    $entity = $items->getEntity();
    $media = $entity instanceof MediaInterface ? $entity : NULL;

    // Require VEF provider manager.
    $vef = $this->vefProviderManager();
    if (!$vef) {
      return;
    }

    $oembed_url_resolver = $this->oEmbedUrlResolver();
    foreach ($items as $delta => $item) {
      $input = trim(strip_tags($item->value ?: ''));

      // Skip empty inputs.
      if (empty($input)) {
        continue;
      }

      // Require a VEF provider.
      $vef_provider = $vef->loadProviderFromInput($input);
      if (empty($vef_provider)) {
        continue;
      }

      // Ensures thumbnail is available.
      $vef_provider->downloadThumbnail();
      $thumbnail_uri = $vef_provider->getLocalThumbnailUri();

      // Update the settings, hard-coded, terracota.
      $sets = $settings;
      $info = [
        'delta' => $delta,
        'image.uri' => $thumbnail_uri,
        'media' => [
          'bundle' => $media ? $media->bundle() : 'remote_video',
          'input_url' => $input,
          'source' => 'video_embed_field',
          'type' => 'video',
        ],
      ];

      $build_settings = $this->formatter->toSettings($sets, $info);

      $data = [
        '#delta' => $delta,
        '#entity' => $entity,
        '#settings' => &$build_settings,
        '#item' => NULL,
      ];

      // Determine if oEmbed can handler this input.
      $oembed_provider = NULL;
      if ($this->getSetting('use_oembed') && $oembed_url_resolver) {
        try {
          $oembed_provider = $oembed_url_resolver->getProviderByUrl($input);
        }
        catch (\Throwable $e) {
          // Suppress errors.
        }
      }

      // Build the blazy embed.
      if ($oembed_provider) {
        // Use the oEmbed provider.
        $this->blazyOembed->build($data);
      }
      else {
        // Use the VEF provider.
        // Get the embed url from the VEF provider.
        $vef_render = $vef_provider->renderEmbedCode(854, 480, '0');
        $embed_url = $input;
        if (!empty($vef_render['#url'])) {
          $vef_url = $vef_render['#url'];
          $vef_url_query = [];
          if (!empty($vef_render['#query'])) {
            $vef_url_query = (array) $vef_render['#query'];
            // Prevents complication with multiple videos.
            unset($vef_url_query['autoplay'], $vef_url_query['auto_play']);
          }
          $embed_url = Url::fromUri($vef_url, ['query' => $vef_url_query])->toString();
        }
        elseif (isset($vef_render['#attributes']) && isset($vef_render['#attributes']['src'])) {
          $embed_url = $vef_render['#attributes']['src'];
        }

        // Bypass Blazy check for oEmbed with "?url".
        if (strpos($embed_url, '?url') === FALSE) {
          if (strpos($embed_url, '?') !== FALSE) {
            $embed_url = str_replace('?', '?url=&', $embed_url);
          }
          else {
            $embed_url .= '?url=';
          }
        }

        $data['#settings']['blazies']->set('media.embed_url', $embed_url);
      }

      // Image with responsive image, lazyLoad, and lightbox supports.
      $build[$delta] = $this->formatter->getBlazy($data);
      unset($data);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'fieldable_form' => TRUE,
      'multimedia'     => TRUE,
    ] + parent::getPluginScopes();
  }

}
